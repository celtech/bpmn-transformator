import dfki.celtech.bpmn2rdf.Bpmn2AppsistOntologyTransformator

/**
 *
 * created by glenn schuetze 
 * 29.07.15
 */

def cli = new CliBuilder()
cli.with {
    usage: 'Self'
    h longOpt:'help', 'usage information'
    i longOpt:'input', 'input file', args:1
    o longOpt:'output', 'output file',args:1
    a longOpt:'action', 'action to invoke',args:1
}


def opt = cli.parse(args)
if( args.length == 0) {
    cli.usage()
    return
}
if( opt.h ) {
    cli.usage()
    return
}

File inputFile = null
if( opt.i ) {
    inputFile = new File( opt.i )

}

File outputFile = null
if ( opt.o ) {
    outputFile = new File( opt.o )
}



// read bpmn file
Bpmn2AppsistOntologyTransformator transformator = Bpmn2AppsistOntologyTransformator.createInstance(
        "digilernpro",
        Bpmn2AppsistOntologyTransformator.NAMESPACE_SEPERATOR,
        Bpmn2AppsistOntologyTransformator.NAME_SEPARATOR,
        "http://www.appsist.de/ontology",
        "APPsist Ontologie",
        true
)

transformator.bpmn2Owl( inputFile, outputFile )

return 0



