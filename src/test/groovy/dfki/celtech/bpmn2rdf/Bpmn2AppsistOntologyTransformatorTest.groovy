package dfki.celtech.bpmn2rdf

import dfki.celtech.bpmn2rdf.types.ElementNode
import dfki.celtech.bpmn2rdf.types.ElementType
import org.apache.commons.io.FilenameUtils

/**
 *
 * created by glenn schuetze 
 * 03.08.15
 */
class Bpmn2AppsistOntologyTransformatorTest extends GroovyTestCase {

    Bpmn2AppsistOntologyTransformator transformator;

    String xmlOutput = """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE rdf:RDF [
\t<!ENTITY terms "http://purl.org/dc/terms/" >
\t<!ENTITY owl "http://www.w3.org/2002/07/owl#" >
\t<!ENTITY ontology "http://www.appsist.de/ontology/" >
\t<!ENTITY dc "http://purl.org/dc/elements/1.1/" >
\t<!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
\t<!ENTITY BPMN "http://www.appsist.de/ontology/BPMN/" >
\t<!ENTITY skos "http://www.w3.org/2004/02/skos/core#" >
\t<!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
\t<!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
]>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns="http://www.appsist.de/ontology/" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:ontology="http://www.appsist.de/ontology/" xml:base="http://www.appsist.de/ontology/" xmlns:BPMN="&amp;ontology;BPMN/" xmlns:terms="http://purl.org/dc/terms/">
  <owl:Ontology rdf:about="http://www.appsist.de/ontology">
    <rdfs:label xml:lang="de">APPsist Ontologie</rdfs:label>
    <owl:versionInfo xml:lang="de">1.0</owl:versionInfo>
  </owl:Ontology>
  <owl:NameIndividual rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process">
    <owl:hatAktivitaet rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten" />
    <owl:hatAktivitaet rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen" />
    <owl:hatAktivitaet rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:Fett wechseln" />
    <owl:hatAktivitaet rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:Station in Betrieb nehmen" />
  </owl:NameIndividual>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten">
    <rdf:label xml:lang="en">Station anhalten</rdf:label>
    <rdf:type rdf:resource="Aktivitaet" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen">
    <rdf:label xml:lang="en">Fett holen</rdf:label>
    <rdf:type rdf:resource="Aktivitaet" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:Fett wechseln">
    <rdf:label xml:lang="en">Fett wechseln</rdf:label>
    <rdf:type rdf:resource="Aktivitaet" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:Station in Betrieb nehmen">
    <rdf:label xml:lang="en">Station in Betrieb nehmen</rdf:label>
    <rdf:type rdf:resource="Aktivitaet" />
  </rdf:Description>
  <owl:NameIndividual rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen">
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen:Verfügbarkeit von Fett prüfen" />
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen:Flaschendeckel abwischen" />
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen:Flasche aus Lager holen" />
  </owl:NameIndividual>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen:Verfügbarkeit von Fett prüfen">
    <rdf:label xml:lang="en">Verfügbarkeit von Fett prüfen</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen:Flaschendeckel abwischen">
    <rdf:label xml:lang="en">Flaschendeckel abwischen</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-fett-holen:Flasche aus Lager holen">
    <rdf:label xml:lang="en">Flasche aus Lager holen</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <owl:NameIndividual rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell">
    <owl:hatSchritt rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell:Schaltfläche &quot;Handschaltung&quot; drücken" />
    <owl:hatSchritt rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell:Monitor anschalten" />
  </owl:NameIndividual>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell:Schaltfläche &quot;Handschaltung&quot; drücken">
    <rdf:label xml:lang="en">Schaltfläche "Handschaltung" drücken</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell:Monitor anschalten">
    <rdf:label xml:lang="en">Monitor anschalten</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <owl:NameIndividual rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten">
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell" />
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Tür öffnen" />
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Luftdruck prüfen" />
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Anlagenführer holen" />
    <owl:hatAktion rdf:resource="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Sicherheitshinweise beachten" />
  </owl:NameIndividual>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:m3-steuerung-manuell">
    <rdf:label xml:lang="en">Steuerung auf manuell stellen</rdf:label>
    <rdf:type rdf:resource="Aktion" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Tür öffnen">
    <rdf:label xml:lang="en">Tür öffnen</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Luftdruck prüfen">
    <rdf:label xml:lang="en">Luftdruck prüfen</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Anlagenführer holen">
    <rdf:label xml:lang="en">Anlagenführer holen</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
  <rdf:Description rdf:about="digilernpro::feac4227-7cab-497b-b1d5-60e7c39bceba:m3-demo-process:m3-station-anhalten:Sicherheitshinweise beachten">
    <rdf:label xml:lang="en">Sicherheitshinweise beachten</rdf:label>
    <rdf:type rdf:resource="ElementarerSchritt" />
  </rdf:Description>
</rdf:RDF>
"""

    String xmlInput = """
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:tns="http://sourceforge.net/bpmn/definitions/_1415274002124" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:yaoqiang="http://bpmn.sourceforge.net" exporter="Yaoqiang BPMN Editor" exporterVersion="4.0" expressionLanguage="http://www.w3.org/1999/XPath" id="_1415274002124" name="" targetNamespace="http://sourceforge.net/bpmn/definitions/_1415274002124" typeLanguage="http://www.w3.org/2001/XMLSchema" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL http://bpmn.sourceforge.net/schemas/BPMN20.xsd">
  <process id="m3-demo-process" isClosed="false" isExecutable="true" processType="None">
    <extensionElements>
      <localData xmlns="appsist:bpmn:annotations">
        <string key="processId">m3-demo-process</string>
        <string key="processName">Fett wechseln</string>
        <string key="processDescription">Beispielprozess zur Demonstration der technischen Plattform.</string>
      </localData>
    </extensionElements>
    <sequenceFlow id="_154" sourceRef="_16" targetRef="_6"/>
    <callActivity calledElement="m3-station-anhalten" completionQuantity="1" id="_156" isForCompensation="false" name="Station anhalten" startQuantity="1">
      <incoming>_195</incoming>
      <outgoing>_21</outgoing>
    </callActivity>
    <sequenceFlow id="_195" sourceRef="_6" targetRef="_156"/>
    <startEvent id="_16" isInterrupting="true" name="Fett Leer" parallelMultiple="false">
      <outgoing>_154</outgoing>
    </startEvent>
    <callActivity calledElement="m3-fett-holen" completionQuantity="1" id="_6" isForCompensation="false" name="Fett holen" startQuantity="1">
      <incoming>_154</incoming>
      <outgoing>_195</outgoing>
    </callActivity>
    <endEvent id="_27" name="End Event">
      <incoming>_20</incoming>
    </endEvent>
    <userTask completionQuantity="1" id="_3" implementation="##unspecified" isForCompensation="false" name="Fett wechseln" startQuantity="1">
      <incoming>_21</incoming>
      <outgoing>_5</outgoing>
    </userTask>
    <userTask completionQuantity="1" id="_4" implementation="##unspecified" isForCompensation="false" name="Station in Betrieb nehmen" startQuantity="1">
      <incoming>_5</incoming>
      <outgoing>_20</outgoing>
    </userTask>
    <sequenceFlow id="_5" sourceRef="_3" targetRef="_4"/>
    <sequenceFlow id="_20" sourceRef="_4" targetRef="_27"/>
    <sequenceFlow id="_21" sourceRef="_156" targetRef="_3"/>
  </process>
  <globalTask id="GT_1" name="Global Task"/>
  <process id="m3-station-anhalten" isClosed="false" isExecutable="true" processType="None">
    <extensionElements>
      <localData xmlns="appsist:bpmn:annotations">
        <string key="processId">m3-station-anhalten</string>
        <string key="processName">Station anhalten</string>
        <string key="processDescription">Anhalten der Station um den Fettwechsel vorzubereiten.</string>
      </localData>
    </extensionElements>
    <userTask completionQuantity="1" id="_172" implementation="##unspecified" isForCompensation="false" name="Tür öffnen" startQuantity="1">
      <incoming>_25</incoming>
      <outgoing>_39</outgoing>
    </userTask>
    <startEvent id="_173" isInterrupting="true" name="Start Event" parallelMultiple="false">
      <outgoing>_22</outgoing>
    </startEvent>
    <callActivity calledElement="m3-steuerung-manuell" completionQuantity="1" id="_178" isForCompensation="false" name="Steuerung auf manuell stellen" startQuantity="1">
      <incoming>_23</incoming>
      <outgoing>_25</outgoing>
    </callActivity>
    <userTask completionQuantity="1" id="_35" implementation="##unspecified" isForCompensation="false" name="Luftdruck prüfen" startQuantity="1">
      <incoming>_39</incoming>
      <outgoing>_40</outgoing>
    </userTask>
    <userTask completionQuantity="1" id="_38" implementation="##unspecified" isForCompensation="false" name="Anlagenführer holen" startQuantity="1">
      <incoming>_41</incoming>
      <outgoing>_42</outgoing>
    </userTask>
    <sequenceFlow id="_39" sourceRef="_172" targetRef="_35"/>
    <sequenceFlow id="_40" sourceRef="_35" targetRef="_37"/>
    <sequenceFlow id="_41" name="Ja" sourceRef="_37" targetRef="_38"/>
    <sequenceFlow id="_42" sourceRef="_38" targetRef="_36"/>
    <sequenceFlow id="_43" name="Nein" sourceRef="_37" targetRef="_171"/>
    <exclusiveGateway gatewayDirection="Diverging" id="_37" name="Druckluft strömt aus?">
      <extensionElements>
        <localData xmlns="appsist:bpmn:annotations">
          <string key="requestMessage">Strömt Druckluft aus?</string>
          <array key="responseOptions">
            <object>
              <string key="display">Ja</string>
              <string key="target">_38</string>
            </object>
            <object>
              <string key="display">Nein</string>
              <string key="target">_171</string>
            </object>
          </array>
        </localData>
        <events xmlns="appsist:bpmn:annotations">
          <event eventId="processEvent:userRequest" type="onStart">
            <properties>
              <entry key="processInstanceId" reference="processInstanceId"/>
              <entry key="processId" reference="processId"/>
              <object key="request">
                <entry key="message" reference="requestMessage"/>
                <entry key="options" reference="responseOptions"/>
              </object>
            </properties>
          </event>
        </events>
      </extensionElements>
      <incoming>_40</incoming>
      <outgoing>_41</outgoing>
      <outgoing>_43</outgoing>
    </exclusiveGateway>
    <sequenceFlow id="_25" sourceRef="_178" targetRef="_172"/>
    <userTask completionQuantity="1" id="_2" implementation="##unspecified" isForCompensation="false" name="Sicherheitshinweise beachten" startQuantity="1">
      <incoming>_22</incoming>
      <outgoing>_23</outgoing>
    </userTask>
    <sequenceFlow id="_22" sourceRef="_173" targetRef="_2"/>
    <sequenceFlow id="_23" sourceRef="_2" targetRef="_178"/>
  </process>
  <process id="m3-steuerung-manuell" isClosed="false" isExecutable="true" processType="None">
    <extensionElements>
      <localData xmlns="appsist:bpmn:annotations">
        <string key="processId">m3-steuerung-manuell</string>
        <string key="processName">Steuerung auf manuell stellen</string>
        <string key="processDescription">Anleitung zum Umschalten der Steuerung auf manuelle Bedienung.</string>
      </localData>
    </extensionElements>
    <userTask completionQuantity="1" id="_180" implementation="##unspecified" isForCompensation="false" name="Schaltfläche &quot;Handschaltung&quot; drücken" startQuantity="1">
      <incoming>_184</incoming>
      <outgoing>_183</outgoing>
    </userTask>
    <userTask completionQuantity="1" id="_181" implementation="##unspecified" isForCompensation="false" name="Monitor anschalten" startQuantity="1">
      <incoming>_185</incoming>
      <outgoing>_184</outgoing>
    </userTask>
    <sequenceFlow id="_183" sourceRef="_180" targetRef="_179"/>
    <sequenceFlow id="_184" sourceRef="_181" targetRef="_180"/>
    <sequenceFlow id="_185" sourceRef="_182" targetRef="_181"/>
  </process>
  <process id="m3-fett-holen" isClosed="false" isExecutable="true" processType="None">
    <extensionElements>
      <localData xmlns="appsist:bpmn:annotations">
        <string key="processId">m3-steuerung-manuell</string>
        <string key="processName">Fett holen</string>
        <string key="processDescription">Hinweise zur Beschaffung.</string>
      </localData>
    </extensionElements>
    <userTask completionQuantity="1" id="_9" implementation="##unspecified" isForCompensation="false" name="Verfügbarkeit von Fett prüfen" startQuantity="1">
      <incoming>_11</incoming>
      <outgoing>_12</outgoing>
    </userTask>
    <sequenceFlow id="_11" sourceRef="_7" targetRef="_9"/>
    <sequenceFlow id="_12" sourceRef="_9" targetRef="_10"/>
    <userTask completionQuantity="1" id="_13" implementation="##unspecified" isForCompensation="false" name="Flaschendeckel abwischen" startQuantity="1">
      <incoming>_14</incoming>
      <incoming>_18</incoming>
      <outgoing>_19</outgoing>
    </userTask>
    <sequenceFlow id="_14" name="Ja" sourceRef="_10" targetRef="_13"/>
    <userTask completionQuantity="1" id="_15" implementation="##unspecified" isForCompensation="false" name="Flasche aus Lager holen" startQuantity="1">
      <incoming>_17</incoming>
      <outgoing>_18</outgoing>
    </userTask>
    <sequenceFlow id="_17" name="Nein" sourceRef="_10" targetRef="_15"/>
    <sequenceFlow id="_18" sourceRef="_15" targetRef="_13"/>
    <sequenceFlow id="_19" sourceRef="_13" targetRef="_8"/>
    <exclusiveGateway gatewayDirection="Diverging" id="_10" name="Fettflasche an Maschine vorhanden?">
      <extensionElements>
        <localData xmlns="appsist:bpmn:annotations">
          <string key="requestMessage">Ist eine Fettflasche an der Maschine vorhanden?</string>
          <array key="responseOptions">
            <object>
              <string key="display">Ja</string>
              <string key="target">_13</string>
            </object>
            <object>
              <string key="display">Nein</string>
              <string key="target">_15</string>
            </object>
          </array>
        </localData>
        <events xmlns="appsist:bpmn:annotations">
          <event eventId="processEvent:userRequest" type="onStart">
            <properties>
              <entry key="processInstanceId" reference="processInstanceId"/>
              <entry key="processId" reference="processId"/>
              <object key="request">
                <entry key="message" reference="requestMessage"/>
                <entry key="options" reference="responseOptions"/>
              </object>
            </properties>
          </event>
        </events>
      </extensionElements>
      <incoming>_12</incoming>
      <outgoing>_14</outgoing>
      <outgoing>_17</outgoing>
    </exclusiveGateway>
  </process>
</definitions>
"""

    void setUp() {
        super.setUp()


        transformator = Bpmn2AppsistOntologyTransformator.createInstance(
                "digilernpro",
                Bpmn2AppsistOntologyTransformator.NAMESPACE_SEPERATOR,
                Bpmn2AppsistOntologyTransformator.NAME_SEPARATOR,
                "http://www.appsist.de/ontology",
                "APPsist Ontologie",
                true
        )

    }

    void testBpmn2Owl() {

        String rdfXml = transformator.bpmn2Owl( xmlInput.toString() )

        println( rdfXml )

    }

    void testNameCleaning() {

        Bpmn2AppsistOntologyTransformator.UMLAUTE = false;

        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "Hello Foo" ) == "Hello Foo"
        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "Hällo Foo" ) == "Haello Foo"
        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "ÄHällo Foo" ) == "AeHaello Foo"
        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "D  D" ) == "D D"

        Bpmn2AppsistOntologyTransformator.UMLAUTE = true;

        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "Hello Foo" ) == "Hello Foo"
        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "Hällo Foo" ) == "Hällo Foo"
        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "ÄHällo Foo" ) == "ÄHällo Foo"
        assert Bpmn2AppsistOntologyTransformator.nameCleaning( "D  D" ) == "D D"

    }

    void testIdCleaning() {

        Bpmn2AppsistOntologyTransformator.UMLAUTE = false;

        assert Bpmn2AppsistOntologyTransformator.idCleaning( "Hello Foo" ) == "Hello_Foo"
        assert Bpmn2AppsistOntologyTransformator.idCleaning( "Hällo Foo" ) == "Haello_Foo"
        assert Bpmn2AppsistOntologyTransformator.idCleaning( "ÄHällo Foo" ) == "AeHaello_Foo"
        assert Bpmn2AppsistOntologyTransformator.idCleaning( "D  D" ) == "D_D"

        Bpmn2AppsistOntologyTransformator.UMLAUTE = true;

        assert Bpmn2AppsistOntologyTransformator.idCleaning( "Hello Foo" ) == "Hello_Foo"
        assert Bpmn2AppsistOntologyTransformator.idCleaning( "Hällo Foo" ) == "Hällo_Foo"
        assert Bpmn2AppsistOntologyTransformator.idCleaning( "ÄHällo Foo" ) == "ÄHällo_Foo"
        assert Bpmn2AppsistOntologyTransformator.idCleaning( "D  D" ) == "D_D"

    }

    void testBuildElementMap() {

        def nodes = [ : ]
        5.times {
            nodes."${it as String}" = new ElementNode( structuralId: "node-${it}" )
            if ( it > 0 ) {
                nodes."${0}".children << nodes."${it}"
                nodes."${it}".parent = nodes."${0}"
            }
            if ( it > 1 ) {
                nodes."${it-1}".followers << nodes."${it}"
                nodes."${it}".predecessor = nodes."${it-1}"
            }
        }

        nodes."${0}".elementType = ElementType.ROOT_PROCESS.elementType


        Map<String,ElementNode> resultMap = transformator.buildElementMap( nodes."${0}", new HashMap<String, ElementNode>() )

        assert resultMap
        assert resultMap.size() == 5

        nodes.keySet().each { key ->

            assert nodes."${key}".structuralId == resultMap."node-${key}".structuralId

        }

    }
}
