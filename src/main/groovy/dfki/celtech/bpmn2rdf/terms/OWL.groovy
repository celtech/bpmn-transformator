package dfki.celtech.bpmn2rdf.terms

/**
 *
 * created by glenn schuetze 
 * 03.08.15
 */
class OWL {

    public static String NameIndividual = "owl:NamedIndividual";
    public static String HatAktiviät    = "owl:hatAktivitaet";
    public static String HatAktion      = "owl:hatAktion";
    // public static String HatSchritt     = "owl:hatSchritt";
    public static String HatSchritt     = "hatSchritt";

    public static String StartElement   = "owl:startElement"
    public static String SequenceFlow   = "owl:sequenceFlow"

    public static String Ontology       = "owl:Ontology";

    public static String VersionInfo    = "owl:versionInfo";

}
