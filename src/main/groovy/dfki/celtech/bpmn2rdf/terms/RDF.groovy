package dfki.celtech.bpmn2rdf.terms

/**
 *
 * created by glenn schuetze 
 * 03.08.15
 */
class RDF {

    public static String about = "rdf:about";
    public static String resource = "rdf:resource";
    public static String description = "rdf:Description";
    public static String label = "rdf:label";
    public static String type = "rdf:type";

}
