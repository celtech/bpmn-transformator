package dfki.celtech.bpmn2rdf

import dfki.celtech.bpmn2rdf.exceptions.MalformedConfigurationException
import dfki.celtech.bpmn2rdf.terms.BPMN
import dfki.celtech.bpmn2rdf.terms.OWL
import dfki.celtech.bpmn2rdf.terms.RDF
import dfki.celtech.bpmn2rdf.terms.RDFS
import dfki.celtech.bpmn2rdf.terms.XML
import dfki.celtech.bpmn2rdf.types.ElementNode
import dfki.celtech.bpmn2rdf.types.ElementType
import dfki.celtech.bpmn2rdf.types.ProcessType
import groovy.util.slurpersupport.GPathResult
import groovy.xml.MarkupBuilder
import groovy.xml.MarkupBuilderHelper
import org.apache.any23.encoding.TikaEncodingDetector
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang.StringEscapeUtils
import org.apache.http.client.utils.URIBuilder

import java.nio.charset.Charset
import java.util.logging.Logger
import java.util.regex.Pattern

/**
 *  this class reads a bpmn xml and transforms it to a rdf xml
 *
 *
 * created by glenn schuetze
 * 29.07.15
 */
class Bpmn2AppsistOntologyTransformator {

    private static Logger log = Logger.getLogger( this.class.name )

    private static Properties rdfTypes;
    private static Properties namespaces;

    public static String NAMESPACE_SEPERATOR = "";
    public static String NAME_SEPARATOR = "/"
    public static String VERSION = "1.0"

    public static boolean UMLAUTE = false;
    public static boolean ECLIPSE_READBALE = true;

    public static Pattern WHITESPACE_PATTERN =  Pattern.compile( "\\s+" );


    public static String NAMESPACE;
    public static String Ontology_Url  = "http://www.appsist.de/ontology/"
    public static String Ontology_Name = "APPsist Ontologie";

    public static String AMP_ONTOLOGY = """&ontology;""";


    /**
     * factory method to get a transformator instance
     *
     * @param namespace
     * @param ontologyUrl
     * @param ontologyName
     * @return Bpmn2AppsistOntologyTransformator instance
     * @throws MalformedConfigurationException if there is an argument null or empty
     * @throws IOException if property files could not be read
     */
    public static Bpmn2AppsistOntologyTransformator createInstance( String namespace,
                                                                    String ontologyUrl,
                                                                    String ontologyName ) throws MalformedConfigurationException, IOException {

        return createInstance(
                namespace,
                NAMESPACE_SEPERATOR,
                NAME_SEPARATOR,
                ontologyUrl,
                ontologyName,
                UMLAUTE,
                ECLIPSE_READBALE
        )
    }



    /**
     * factory method to get a transformator instance
     *
     * @param namespace
     * @param namespaceSeparator
     * @param nameSeparator
     * @param ontologyUrl
     * @param ontologyName
     * @param eclipseReadable
     * @return Bpmn2AppsistOntologyTransformator instance
     * @throws MalformedConfigurationException if there is an argument null or empty
     * @throws IOException if property files could not be read
     */
    public static Bpmn2AppsistOntologyTransformator createInstance( String  namespace,
                                                                    String  namespaceSeparator,
                                                                    String  nameSeparator,
                                                                    String  ontologyUrl,
                                                                    String  ontologyName,
                                                                    boolean umlaute,
                                                                    boolean eclipseReadable ) throws MalformedConfigurationException, IOException {

        return new Bpmn2AppsistOntologyTransformator(
                namespace,
                namespaceSeparator,
                nameSeparator,
                ontologyUrl,
                ontologyName,
                umlaute,
                eclipseReadable
        )

    }

    /**
     * private Constructor, use factory methods instead
     *
     * @param namespace
     * @param namespaceSeparator
     * @param nameSeparator
     * @param ontologyUrl
     * @param ontologyName
     * @throws MalformedConfigurationException if there is an argument null or empty
     * @throws IOException if property files could not be read
     */
    private Bpmn2AppsistOntologyTransformator( String  namespace,
                                               String  namespaceSeparator,
                                               String  nameSeparator,
                                               String  ontologyUrl,
                                               String  ontologyName,
                                               boolean umlaute,
                                               boolean eclipseReadable ) throws MalformedConfigurationException, IOException {

        NAMESPACE_SEPERATOR = namespaceSeparator;
        NAME_SEPARATOR = nameSeparator;

        NAMESPACE = namespace + NAMESPACE_SEPERATOR ;

        Ontology_Url = ontologyUrl;
        Ontology_Name = ontologyName

        rdfTypes = readRdfTypes();
        namespaces = readNamespaces();

        UMLAUTE = umlaute;

        ECLIPSE_READBALE = eclipseReadable;

        checkNull( namespace, "namespace" );
        checkNull( namespaceSeparator, "namespaceSeparator" );
        checkNull( nameSeparator, "nameSeparator" );
        checkNull( ontologyUrl, "ontologyUrl" );
        checkNull( ontologyName, "ontologyName" );

    }


    /**
     * helüer method for instance creation, checks if a string is null
     * and throws an exception
     *
     * @param object
     * @param name
     * @throws MalformedConfigurationException if object is null or empty
     */
    private static checkNull( String object, String name ) throws MalformedConfigurationException {

        if ( object == null ) {
            throw new MalformedConfigurationException( "${name} is null" )
        }

    }



    public String bpmn2Owl( String inputBpmn ) {

        /**
         * parse bpmn xml and hold the structure as rootNode
         */
        GPathResult rootNode = new XmlSlurper().parseText( inputBpmn );

        String rootProcess = getRootProcessId( rootNode )

        Map<String,ElementNode> elementNodes = new HashMap<String,ElementNode>()
        ElementNode nnn = createRootNode( rootProcess, rootNode, true, elementNodes )



        return buildXmlString( nnn )
    }

    /**
     * this method reads xml from inputfile, transforms it into a tree of
     * ElementNodes and returns the root element
     *
     * @param inputFile
     * @return
     */
    public ElementNode getBpmnTree( File inputFile ) {

        return getBpmnTree(  readTextFromFile( inputFile )  )

    }

    /**
     * this method parses inputXml and transforms it into a tree of
     * ElementNodes and returns the root element
     *
     * @param inputXml
     * @return
     */
    public ElementNode getBpmnTree( String inputXml ) {

        GPathResult rootNode = new XmlSlurper().parseText( inputXml );

        String rootProcess = getRootProcessId( rootNode )

        Map<String,ElementNode> elementNodes = new HashMap<String,ElementNode>()

        return createRootNode( rootProcess, rootNode, true, elementNodes )
    }


    /**
     * this method reads from bpmnFile (by guessing charset), parses
     * bpmn content and transforms content into owl
     *
     * @param bpmnFile
     * @return a String in owl
     */
    public String bpmn2Owl( File bpmnFile ) {

        return bpmn2Owl( readTextFromFile( bpmnFile ) );

    }

    /**
     * this method reads from bpmnFile (by guessing charset), parses
     * bpmn content and transforms content into owl and
     * writes transformation result into given outputFile
     *
     * @param bpmnFile
     * @param outputFile
     * @return a file with owl content
     * @throws IOException
     */
    public File bpmn2Owl( File bpmnFile, File outputFile ) throws IOException {

        /**
         * read, parse string from file and transform into rdf-xml string
         */
        String rdfXmlString = bpmn2Owl( bpmnFile );

        /**
         * write rdf-xml string into given file
         */
        outputFile.withWriter { it << rdfXmlString }

        return outputFile;
    }



    private ElementNode createRootNode( String id, GPathResult xmlRootNode, boolean isRootNode, Map<String,ElementNode> elementNodes ) {

        ElementNode currentRootNode = null;

        if ( isRootNode ) {
            currentRootNode = new ElementNode(

                    id: id,
                    structuralId: id,
                    parent: null,
                    elementType: ElementType.ROOT_PROCESS.elementType,
                    uuid: UUID.randomUUID().toString()
            )

            elementNodes.put( "root", currentRootNode )
        } else {

            currentRootNode = elementNodes.get( id )

        }


        if ( currentRootNode ) {

            /**
             * find all 'processes'
             */
            xmlRootNode.process.each { p ->

                /**
                 * root node detection
                 */
                if ( p.@id == currentRootNode.id ) {

                    if ( isRootNode ) {
                        currentRootNode.name = p.extensionElements.'**'.find { s -> s.@key == "processName" }.toString()
                        currentRootNode.description = p.extensionElements.'**'.find { s -> s.@key == "processDescription" }.toString()
                    }


                    // each callActivity is created as childrenNode
                    p.callActivity.each { callActivity ->

                        // try to pick the callActivitie's name here if there is one
                        ElementNode node = new ElementNode(
                                id: callActivity.@calledElement,
                                elementType: ElementType.PROCESS.elementType,
                                parent: currentRootNode,
                                name: callActivity.@name,
                                structuralId: callActivity.@id
                        )
                        elementNodes.put( callActivity.@id as String, node )
                        currentRootNode.children << node

                    }

                    p.userTask.each { userTask ->
                        ElementNode node = new ElementNode(
                                id: userTask.@name,
                                elementType: ElementType.ATOMIC_ACTION.elementType,
                                parent: currentRootNode,
                                name: userTask.@name,
                                structuralId: userTask.@id
                        )
                        elementNodes.put( userTask.@id as String, node )
                        currentRootNode.children << node
                    }

                    // there should be one StartEvent
                    p.startEvent.each { startEvent ->
                        ElementNode node = new ElementNode(
                                id: startEvent.@name,
                                elementType: ElementType.STARTEVENT.elementType,
                                parent: currentRootNode,
                                name: startEvent.@name,
                                structuralId: startEvent.@id
                        )
                        elementNodes.put( startEvent.@id as String, node )
                        currentRootNode.children << node
                    }

                    // Might be more than one endEvent
                    p.endEvent.each { endEvent ->
                        ElementNode node = new ElementNode(
                                id: endEvent.@name,
                                elementType: ElementType.ENDEVENT.elementType,
                                parent: currentRootNode,
                                name: endEvent.@name,
                                structuralId: endEvent.@id
                        )
                        elementNodes.put( endEvent.@id as String, node )
                        currentRootNode.children << node
                    }

                    p.exclusiveGateway.each { gateway ->
                        ElementNode node = new ElementNode(
                                id: gateway.@name,
                                name: gateway.@name,
                                condition: gateway.@name, // same as name TODO: skip condition ?
                                structuralId: gateway.@id,
                                parent: currentRootNode,
                                elementType: ElementType.GATEWAY.elementType
                        )
                        elementNodes.put( gateway.@id as String, node )
                        currentRootNode.children << node
                    }


                    // collectiong sequence elements
                    p.sequenceFlow.each { sequenceFlow ->

                        ElementNode node = new ElementNode(
                                id:   sequenceFlow.@id as String ,
                                name: sequenceFlow.@name as String  ,
                                structuralId: sequenceFlow.@id as String,
                                parent: currentRootNode,
                                predecessor: elementNodes.get( sequenceFlow.@sourceRef as String ),
                                elementType: ElementType.SEQUENCE.elementType
                        )

                        ElementNode predecessor = elementNodes.get( sequenceFlow.@sourceRef as String )
                        ElementNode follower = elementNodes.get( sequenceFlow.@targetRef as String )

                        if ( follower ) {
                            node.followers << follower
                            follower.predecessor = node
                        }

                        // if list of predecessor's followers already contains current node, skip
                        // otherwise add it
                        if ( predecessor && !predecessor.followers.find { it.structuralId == node.structuralId } ) {

                            predecessor.followers << node

                        }

                        elementNodes.put( sequenceFlow.@id as String, node )
                        currentRootNode.children << node

                    }

                }


            }

            for ( ElementNode childNode : currentRootNode.children ) {

                // String id, GPathResult xmlRootNode, boolean isRootNode, Map<String,ElementNode> elementNodes
                ElementNode lll = createRootNode( childNode.structuralId, xmlRootNode, false, elementNodes )

            }

        }




        return currentRootNode

    }





    /**
     * given a parent node's type, this method decides for a
     * children's node type
     *
     * the decision process is bottom up.
     *
     * // TODO: rethink! what is "elemtarer schritt". could it be a child of "massnahme"?
     *
     * @param parentType
     * @return a parentType-level - 1
     */
    private String getOwlType( String parentType ) {

        String hasWHAT = null;
        switch ( parentType ) {
            case ProcessType.MASSNAHME.processType :
                hasWHAT = OWL.HatAktiviät;
                break;
            case ProcessType.AKTIVITAET.processType :
                hasWHAT = OWL.HatAktion;
                break;
            case ProcessType.AKTION.processType :
                hasWHAT = OWL.HatSchritt;
                break;
            default:
                break;

        }

        return hasWHAT;
    }



    /**
     * creates xml string by using MarkupBuilder DSL
     *
     * @param rootNode
     * @return
     */
    private  String buildXmlString( ElementNode rootNode ) {

        def writer = new StringWriter()
        def xml = new MarkupBuilder( writer )

        String ampOnt = ECLIPSE_READBALE?AMP_ONTOLOGY:"";

        Map<String,ElementNode> elementMap = new HashMap<String,ElementNode>()
        elementMap = buildElementMap( rootNode, elementMap );

        def helper = new MarkupBuilderHelper( xml )
        xml.doubleQuotes = true

        helper.xmlDeclaration( [ version: "1.0", encoding: "UTF-8" , standalone: "no" ])
        helper.yieldUnescaped """<!DOCTYPE rdf:RDF [
\t<!ENTITY terms "http://purl.org/dc/terms/" >
\t<!ENTITY owl "http://www.w3.org/2002/07/owl#" >
\t<!ENTITY ontology "http://www.appsist.de/ontology/" >
\t<!ENTITY dc "http://purl.org/dc/elements/1.1/" >
\t<!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
\t<!ENTITY BPMN "http://www.appsist.de/ontology/BPMN/" >
\t<!ENTITY skos "http://www.w3.org/2004/02/skos/core#" >
\t<!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
\t<!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
]>\n"""

        xml.'rdf:RDF'(
                namespaces
        ) {

            "${OWL.Ontology}"( "${RDF.about}" : "${Ontology_Url}" ) {

                "${RDFS.label}"( "${XML.lang}" : "de", Ontology_Name )
                "${OWL.VersionInfo}"( "${XML.lang}" : "de", VERSION );

            }


            // that's root, everything clear for this block!
            "${OWL.NameIndividual}"( "${RDF.about}" : ampOnt + rootNode.rdfId ) {


                "${RDF.type}"( "${RDF.resource}" :  ampOnt + ElementType.MASSNAHME.elementType )

                // finding start element

                "${BPMN.StartElement}"( "${RDF.resource}" : ampOnt + rootNode.children.find {
                    it.elementType == ElementType.STARTEVENT.elementType
                }.followers?.get( 0 )?.followers?.get( 0 )?.rdfId )
                /*
                "${OWL.StartElement}"( "${RDF.resource}" : ampOnt + rootNode.children.find {
                    it.elementType == ElementType.STARTEVENT.elementType
                }.followers?.get( 0 )?.followers?.get( 0 )?.rdfId )
                */


                for ( ElementNode activityNode : rootNode.children ) {

                    if ( activityNode.elementType == ElementType.PROCESS.elementType
                            || activityNode.elementType == ElementType.ATOMIC_ACTION.elementType ) {


                        "${OWL.HatSchritt}"( "${RDF.resource}" : ampOnt + activityNode.rdfId )

                    }

                }

            }

            /**
             * descriptions
             */
            for ( ElementNode activityNode : rootNode.children ) {

                if ( activityNode.elementType == ElementType.PROCESS.elementType
                        || activityNode.elementType == ElementType.ATOMIC_ACTION.elementType ) {



                    "${RDF.description}"( "${RDF.about}" : ampOnt + activityNode.rdfId ) {

                        "${RDFS.label}"( "${XML.lang}" : "de", "${activityNode.name}" )

                        "${RDF.type}"( "${RDF.resource}" : ampOnt + ProcessType.AKTIVITAET.processType )

                        // get 0 get 0 -> TODO: gateways ??
                        "${BPMN.SequenceFlow}"( "${RDF.resource}" : ampOnt + activityNode.followers.get( 0 ).followers.get( 0 ).rdfId )

                    }


                }

            }



            /**
             * for each process which has Children and is not the root process
             * gets an entry
             */
           for ( ElementNode node : elementMap.values() ) {

               if ( node.parent != null && node.children.size() > 0 ) {

                   /**
                    * The individualName definition block
                    */
                   "${OWL.NameIndividual}"( "${RDF.about}" : ampOnt + node.rdfId ) {

                       // finding start element
                       "${BPMN.StartElement}"( "${RDF.resource}" : ampOnt + node.children.find {
                           it.elementType == ElementType.STARTEVENT.elementType
                       }.followers?.get( 0 )?.followers?.get( 0 )?.rdfId )


                       /**
                        * individual name
                        */
                       for ( ElementNode activityNode : node.children.findAll { it.elementType == ElementType.PROCESS.elementType ||it.elementType == ElementType.ATOMIC_ACTION.elementType } ) {


                           "${OWL.HatSchritt}"( "${RDF.resource}" : ampOnt + activityNode.rdfId )

                       }

                   }

                   /**
                    * descriptions
                    */
                   for ( ElementNode activityNode : node.children.findAll { it.elementType == ElementType.PROCESS.elementType } ) {

                       "${RDF.description}"( "${RDF.about}" : ampOnt + activityNode.rdfId ) {

                           "${RDFS.label}"( "${XML.lang}" : "de", "${activityNode.name}" )

                           "${RDF.type}"( "${RDF.resource}" : ampOnt + ProcessType.AKTIVITAET.processType )

                       }

                   }


                   /**
                    * descriptions
                    */
                   for ( ElementNode activityNode : node.children.findAll { it.elementType == ElementType.ATOMIC_ACTION.elementType } ) {

                       "${RDF.description}"( "${RDF.about}" : ampOnt + activityNode.rdfId ) {

                           "${RDFS.label}"( "${XML.lang}" : "de", "${activityNode.name}" )

                           // "${RDF.type}"( "${RDF.resource}" : ampOnt + ProcessType.BENUTZER_TASK.processType )
                           "${RDF.type}"( "${RDF.resource}" : ampOnt + ProcessType.AKTIVITAET.processType )


                       }

                   }

               }

           }

        }

        return StringEscapeUtils.unescapeXml( writer.toString() )  ;

    }





    /**
     * find the unique process name, which is called from no other process
     *
     * if there is exactly one, return the name
     * null otherwise
     *
     * @param rootNode
     * @return
     */
    private   String getRootProcessId( GPathResult rootNode ) {

        /**
         * set difference: 'all processes' / 'called processes' should result in 1 process
         */
        def rootProcessList = rootNode.process*.@id.toSet() - rootNode.'**'.findAll { it.@calledElement?.size() > 0 }*.@calledElement.toSet()

        if ( rootProcessList.size() == 1 ) {
            return rootProcessList[ 0 ]
        } else {
            return null
        }
    }

    /**
     * takes file descriptor, tries to read from underlying file by guessing
     * charset
     *
     * @param inputFile
     * @return String content of file
     * @throws IOException
     * @throws FileNotFoundException
     */
    private String readTextFromFile( File inputFile ) throws IOException, FileNotFoundException {

        inputFile.getText(  guessCharset( new FileInputStream( inputFile ) ).name()  )

    }





    /**
     * recursively puts all element nodes into a Map
     *
     * @param rootNode
     * @param processMap
     * @return a map filled with existent entries and with added current process all identified by process id
     */
    private Map<String,ElementNode> buildElementMap( ElementNode rootNode, Map<String,ElementNode> elementMap ) {

        elementMap.put( rootNode.structuralId, rootNode );
        for ( ElementNode cNode : rootNode.children ) {
            elementMap = buildElementMap( cNode, elementMap )
        }

        return elementMap
    }




    /**
     *
     * @return all properties from  namespaces.properties - file
     */
    private Properties readNamespaces() throws IOException {
        namespaces = new Properties()
        ClassLoader classLoader = getClass().getClassLoader();
        File propsFile = new File( classLoader.getResource("namespaces.properties").getFile() )

        try {
            namespaces.load(propsFile.newDataInputStream())
        } catch ( IOException ioe ) {
            log.info( "failed to read namespace properties from file ${propsFile.absolutePath} - ${ioe}" )
        }

        return namespaces;
    }

    /**
     *
     * @return all properties from   rdftypes.properties - file
     */
    private Properties readRdfTypes() throws IOException {
        rdfTypes = new Properties()
        ClassLoader classLoader = getClass().getClassLoader();

        File propsFile = new File( classLoader.getResource("rdftypes.properties").getFile() )

        rdfTypes.load(propsFile.newDataInputStream())

        return rdfTypes
    }


    public static Charset guessCharset( InputStream is ) throws IOException {
        return  Charset.forName(new TikaEncodingDetector().guessEncoding(is));
    }



    public static String nameCleaning( String s ) {

        s = WHITESPACE_PATTERN.matcher( s ).replaceAll( " " );

        return s;

    }

    public static whiteSpaceCleaning( String s ) {
        return WHITESPACE_PATTERN.matcher( s ).replaceAll( " " );
    }

    public static String umlauteCleaning( String s ) {

        if ( !UMLAUTE ) {
            s = s.replace( "ä", "ae" );
            s = s.replace( "ö", "oe" );
            s = s.replace( "ü", "ue" );
            s = s.replace( "ß", "ss" );
            s = s.replace( "Ä", "Ae" );
            s = s.replace( "Ö", "Oe" );
            s = s.replace( "Ü", "Ue" );
        }

        return s;
    }


    public static String idCleaning( String s ) {

        return URLEncoder.encode( WHITESPACE_PATTERN.matcher( umlauteCleaning( whiteSpaceCleaning( s ) ) ).replaceAll( "_" ), "UTF-8" );

    }

    public static void main( String... args ) {
        Bpmn2AppsistOntologyTransformator transformator = Bpmn2AppsistOntologyTransformator.createInstance(
                "",
                Bpmn2AppsistOntologyTransformator.NAMESPACE_SEPERATOR,
                Bpmn2AppsistOntologyTransformator.NAME_SEPARATOR,
                "http://www.appsist.de/ontology",
                "APPsist Ontologie",
                false,
                true
        )

        // File file = new File( "/Users/glsc01/Documents/dfki/DIGILERNPRO/Skripte/groovy/BNMP2RDF/src/LoctiteWechseln.bpmn" );
        File file = new File( "/Users/glenn/Desktop/20150805-Fehlendes-Bauteil.bpmn" );


        String s = transformator.bpmn2Owl( file )
        println( s )


        File outFile = transformator.bpmn2Owl( file, new File( "/Users/glenn/Desktop/${FilenameUtils.removeExtension( file.name )}.owl" ) )



    }


}
