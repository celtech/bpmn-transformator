package dfki.celtech.bpmn2rdf.types

/**
 *
 * created by glenn schuetze 
 * 05.08.15
 */
enum ElementType {

    GATEWAY( 'Gateway', 'hua1' ),
    ROOT_PROCESS( 'RootProcess', 'hua34' ),
    PROCESS( 'Process', 'hua2' ),
    ATOMIC_ACTION( 'Atomic Action', 'hua22' ),
    STARTEVENT( 'StartEvent', 'hua3' ),
    ENDEVENT( 'EndEvent', 'hua4' ),
    SEQUENCE( 'SEQUENCE', 'hua5' ),
    MASSNAHME( 'Massnahme', 'cf' )


    String elementType
    String huaType

    ElementType ( String elementType, String huaType ) {
        this.huaType = huaType;
        this.elementType = elementType;
    }

    static ElementType findByElementType( String elementType ) {
        values().find { it.elementType.equals( elementType ) }
    }

}