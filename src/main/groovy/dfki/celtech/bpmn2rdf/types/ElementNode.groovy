package dfki.celtech.bpmn2rdf.types

import dfki.celtech.bpmn2rdf.Bpmn2AppsistOntologyTransformator

/**
 *
 * created by glenn schuetze 
 * 05.08.15
 */
class ElementNode {

    /**
     * attributes
     */
    String id
    String structuralId // root nodes doesn't have this
    String name
    String description

    // only for root nodes
    String uuid

    /**
     * the type found in xml ( 'userTask', 'manualTask', 'exclusiveGateway', ... )
     */
    String foundType

    /**
     * types
     */
    // like Gateway, Action, Startevent, Sequence
    String elementType

    /**
     * structural attributes
     */

    /**
     * is null, iff it is the root node
     */
    ElementNode parent

    /**
     * one level deeper
     */
    List<ElementNode> children = new ArrayList<ElementNode>()

    /**
     * normally size is 1 or 0
     * if this.'elementType' is Pfeil AND this.'predecessor'.'elementType' is 'Gateway'
     *  -> might be more than one
     */
    List<ElementNode> followers = new ArrayList<ElementNode>()

    /**
     * if it is the first node in the level, predecessor is null
     */
    ElementNode predecessor


    /**
     * only for Gateway nodes
     */
    String condition


    /**
     * only for arrows which predecessor is a gateway node
     */
    String branch






    /**
     * for root nodes a name is provided consisting of
     *      a) Namespace
     *      b) uuid for the tree
     *      c) id of the node
     *
     * for all other nodes, the name is build recursively
     *
     * @return a qualified name for ingesting into rdf store
     */
    String getRdfId() {

        /**
         * for root nodes
         */
        if ( parent == null ) {
            return Bpmn2AppsistOntologyTransformator.NAMESPACE + uuid.toString() + Bpmn2AppsistOntologyTransformator.NAME_SEPARATOR + Bpmn2AppsistOntologyTransformator.idCleaning( id )
        }

        /**
         * for all non roots
         */
        return parent.rdfId + Bpmn2AppsistOntologyTransformator.NAME_SEPARATOR + Bpmn2AppsistOntologyTransformator.idCleaning( id )

    }


    String getName() {

        return Bpmn2AppsistOntologyTransformator.whiteSpaceCleaning( name )

    }


    String getDescription() {

        return Bpmn2AppsistOntologyTransformator.whiteSpaceCleaning( description )

    }

}
