package dfki.celtech.bpmn2rdf.types

/**
 *
 * created by glenn schuetze 
 * 03.08.15
 */
enum ProcessType {

    MASSNAHME( 'Massnahme', 'hua1' ),
    AKTIVITAET( 'Aktivitaet', 'hua2' ),
    AKTION( 'Aktion', 'hua3' ),
    ELEMENTARER_SCHRITT( 'ElementarerSchritt', 'hua4' ),
    BENUTZER_TASK( 'UserTask', 'hua4324' )

    String processType
    String huaType

    ProcessType ( String processType, String huaType ) {
        this.huaType = huaType;
        this.processType = processType;
    }

    static ProcessType findByProcessType( String processType ) {
        values().find { it.processType.equals( processType ) }
    }

}