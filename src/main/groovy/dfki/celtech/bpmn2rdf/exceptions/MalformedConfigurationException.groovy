package dfki.celtech.bpmn2rdf.exceptions

/**
 *
 * created by glenn schuetze 
 * 03.08.15
 */
class MalformedConfigurationException extends Exception {

    public MalformedConfigurationException() {
        super();
    }

    public MalformedConfigurationException(String message) {
        super(message);
    }

    public MalformedConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public MalformedConfigurationException(Throwable cause) {
        super(cause);
    }

}
